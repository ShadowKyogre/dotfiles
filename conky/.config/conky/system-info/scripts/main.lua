require 'cairo'
require 'cairo_imlib2_helper'
require 'math'
require 'os'

package.path = package.path .. ';' .. os.getenv('HOME') .. '/.config/conky/conky-utils/?.lua'
require 'graphics'

function draw_rectangle(cr, params)
    cairo_save(cr)

    if params.shape == 'rectangle' then
        create_rectangle(cr, params)
    elseif params.shape == 'rounded-rectangle' then
        create_rounded_rectangle(cr, params)
    else
        create_rectangle(cr, params)
    end

    bg_pattern = pattern_cache[params.pc_prefix .. '_bg']
    bg_pattern_cached = (pattern_cache[params.pc_prefix .. '_bg'] ~= nil)
    if not bg_pattern_cached then
        bg_pattern = pattern_factory(params, params.bg)
        pattern_cache[params.pc_prefix .. '_bg'] = bg_pattern
    end
    if bg_pattern ~= nil then
        cairo_set_source(cr, bg_pattern)
        cairo_fill_preserve(cr)
        -- cairo_pattern_destroy(bg_pattern)
    end

    if params.outline.width > 0 then
        outline_pattern = pattern_cache[params.pc_prefix .. '_outline']
        outline_pattern_cached = (pattern_cache[params.pc_prefix .. '_outline'] ~= nil)
        if not outline_pattern_cached then
            outline_pattern = pattern_factory(params, params.outline)
            pattern_cache[params.pc_prefix .. '_outline'] = outline_pattern
        end

        if outline_pattern ~= nil then
            cairo_set_line_width(cr, params.outline.width)
            cairo_set_source(cr, outline_pattern)
            cairo_stroke(cr)
            -- cairo_pattern_destroy(outline_pattern)
        end
    end

    cairo_restore(cr)
end

function conky_startup()
    config = {
        logo   = {x = 48, y =  48,  w = 16,  h = 16},
        avatar = {x = 0,  y =   0,  w = 64,  h = 64},
        system = {x = 0,  y =  72,  w = 256, h = 104},
    }

    print("Initializing pattern palette...")
    pattern_cache = {}

    print("Initializing image cache...")
    img_cache = {}
    imgs = {
        avatar = (os.getenv('HOME') .. '/.face'),
        logo   = '/usr/share/icons/artix/logo.svg',
    }
    img_cache[imgs.avatar] = surface_from_png(imgs.avatar, {w=config.avatar.w, h=config.avatar.h, smart=true})
    img_cache[imgs.logo]   = surface_from_svg(imgs.logo,   {w=config.logo.w, h=config.logo.h, smart=true})
end

function conky_shutdown()
    -- TODO: this is specifically NOT called when the lua script is edited, but not the main config!
    print("Tearing down pattern palette...")
    for key, pattern in pairs(pattern_cache) do
        cairo_pattern_destroy(pattern_cache)
        pattern_cache[key] = nil
        print("Ripping up " .. key)
    end
    print("Tearing down image cache...")
    for key, value in pairs(imgs) do
        cairo_surface_destroy(img_cache[value])
        img_cache[value] = nil
        print("Ripping up " .. value)
    end
    print("Done tearing up!")
end

function conky_main()
    if conky_window == nil then
        return
    end

    if conky_window.width <= 0 or conky_window.height <= 0 then
        return
    end

    local cs = cairo_xlib_surface_create (conky_window.display,
                                         conky_window.drawable,
                                         conky_window.visual,
                                         conky_window.width,
                                         conky_window.height)
    cr = cairo_create (cs)

    cairo_save(cr)
    local avvie = img_cache[imgs.avatar]
    local avvie_ref_count = cairo_surface_get_reference_count(avvie)
    if avvie_ref_count == 0 then
        img_cache[imgs.avatar] = surface_from_png(imgs.avatar, {w=config.avatar.w, h=config.avatar.h, smart=true})
        avvie = img_cache[imgs.avatar]
    end
    cairo_set_source_surface(cr, avvie, config.avatar.x, config.avatar.y)
    cairo_paint_with_alpha(cr, 1)
    cairo_restore(cr)

    cairo_save(cr)
    local logo = img_cache[imgs.logo]
    local logo_ref_count = cairo_surface_get_reference_count(logo)
    if logo_ref_count == 0 then
        img_cache[imgs.logo]   = surface_from_svg(imgs.logo,   {w=config.logo.w, h=config.logo.h, smart=true})
        logo = img_cache[imgs.logo]
    end
    cairo_set_source_surface(cr, logo, config.logo.x, config.logo.y)
    cairo_paint_with_alpha(cr, 1)
    cairo_restore(cr)

    -- Actual drawing logic goes into a separate function
    cairo_save(cr)
    cairo_translate(cr, config.system.x, config.system.y)
    main_panel_params = {
        pc_prefix = "main_panel",
        shape = "rectangle",
        bg={
            kind="pattern",
            subkind="linear",
            stops={
                {offset=0.0, r=0, b=0, g=0, a=0.5},
                {offset=1.0, r=0, b=1, g=0, a=0.5},
            },
        },
        outline={
            kind="color",
            width=2,
            r=1, b=1, g=1, a=0.5
        },
        x1=0,
        y1=0,
        x2=config.system.w,
        y2=config.system.h,
    }
    draw_rectangle(cr, main_panel_params)

    header_params = {
        pc_prefix = "main_header",
        shape = "rectangle",
        bg={
            kind="pattern",
            subkind="linear",
            orientation="top",
            stops={
                {offset=0.0, r=0, b=0.5, g=0, a=1.0},
                {offset=1.0, r=0, b=1.0, g=0, a=1.0},
            },
        },
        outline={
            kind="color",
            width=0,
            r=1, b=1, g=1, a=0.5
        },
        x1=2 * main_panel_params.outline.width,
        y1=2 * main_panel_params.outline.width,
        x2=config.system.w - 2 * main_panel_params.outline.width,
        y2=24 + 2 * main_panel_params.outline.width,
    }
    draw_rectangle(cr, header_params)
    cairo_restore(cr)

    cairo_destroy (cr)
    cairo_surface_destroy(cs)
    cr = nil
end

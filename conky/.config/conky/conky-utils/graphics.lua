require 'math'
require 'rsvg'

function blend_colors(c1, c2, blend_factor)
    local result = {
        r=math.sqrt( (1 - blend_factor) * c1.r^2 + blend_factor * c2.r^2),
        g=math.sqrt( (1 - blend_factor) * c1.g^2 + blend_factor * c2.g^2),
        b=math.sqrt( (1 - blend_factor) * c1.b^2 + blend_factor * c2.b^2),
        a=math.sqrt( (1 - blend_factor) * c1.a^2 + blend_factor * c2.a^2),
    }
    return result
end

function surface_from_png(file, setup)
  local cs = cairo_image_surface_create_from_png(file)
  local out_cs = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, setup.w, setup.h)
  local png_cr = cairo_create(out_cs)
  local smart = setup.smart or false

  if smart then
      local w_ratio = 1
      local h_ratio = 1
      local xoffset = 0
      local yoffset = 0
      if setup.w or setup.h then -- want horizontal or vertical scaling, keep proportional
          local w =  cairo_image_surface_get_width(cs)
          local h =  cairo_image_surface_get_height(cs)
          if w > h then
              w_ratio = setup.w and setup.w / w or 1
              h_ratio = w_ratio
              yoffset = (setup.w * (h - h_ratio)) / 2
          else
              h_ratio = setup.h and setup.h / h or 1
              w_ratio = h_ratio
              xoffset = (setup.h - (w * w_ratio)) / 2
          end
          cairo_translate(png_cr, xoffset, yoffset)
          cairo_scale(png_cr, w_ratio, h_ratio)
      end
      cairo_set_source_surface(png_cr, cs, 0, 0)
  else
      if setup.w or setup.h then -- want horizontal or vertical scaling
        local w =  cairo_image_surface_get_width(cs)
        local h =  cairo_image_surface_get_height(cs)
        local w_ratio = setup.w and setup.w / w or 1
        local h_ratio = setup.h and setup.h / h or 1
        -- x = x / w_ratio
        -- y = y / h_ratio
        cairo_scale(png_cr, w_ratio, h_ratio)
      end
      cairo_set_source_surface(png_cr, cs, 0, 0)
  end

  cairo_paint_with_alpha(png_cr, setup.a or 1)
  cairo_destroy(png_cr)
  return out_cs
end

function surface_from_svg(file, params)
    local handle = rsvg_handle_new_from_file(file)
    local rect = RsvgRectangle:create()
    tolua.takeownership(rect)
    local w = params.w
    local h = params.h
    rect:set(0, 0, w, h)

    local err
    local surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h)
    local svg_cr = cairo_create(surface)

    local render_result = rsvg_handle_render_document(handle, svg_cr, rect, err)
    tolua.releaseownership(rect)
    RsvgRectangle:destroy(rect)
    cairo_destroy(svg_cr)

    return surface
end

function create_rectangle(cr, params)
    cairo_new_sub_path(cr)
    cairo_move_to(cr, params.x1 + 1 * params.outline.width, params.y1 + 1 * params.outline.width)
    cairo_line_to(cr, params.x1 + 1 * params.outline.width, params.y2 - 1 * params.outline.width)
    cairo_line_to(cr, params.x2 - 1 * params.outline.width, params.y2 - 1 * params.outline.width)
    cairo_line_to(cr, params.x2 - 1 * params.outline.width, params.y1 + 1 * params.outline.width)
    cairo_close_path(cr)
end

-- source: https://www.cairographics.org/samples/rounded_rectangle/
function create_rounded_rectangle(cr, params)
    x = params.x1 + 1 * params.outline.width
    y = params.y1 + 1 * params.outline.width
    width = params.x2 - params.x1 - 2 * params.outline.width
    height = params.y2 - params.y1 - 2 * params.outline.width
    radius = params.radius - 1 * params.outline.width

    cairo_new_sub_path (cr)
    cairo_arc (cr, x + width - radius, y + radius, radius, math.rad(-90), math.rad(0))
    cairo_arc (cr, x + width - radius, y + height - radius, radius, math.rad(0), math.rad(90))
    cairo_arc (cr, x + radius, y + height - radius, radius, math.rad(90), math.rad(180))
    cairo_arc (cr, x + radius, y + radius, radius, math.rad(180), math.rad(270))
    cairo_close_path (cr)
end

function pattern_factory(params, subparams)
    pattern = nil
    if subparams.subkind == "radial" then
        radial_params = {}
        -- centered orientation for now
        if subparams.orientation == "center" then
            radial_params = {
                params.x2 / 2, params.y2 / 2,
                0,
                params.x2 / 2, params.y2 / 2,
                math.max(params.x2 - params.x1, params.y2 - params.y1) / 2
            }
        elseif subparams.orientation == "top" then
            radial_params = {
                params.x1 + (params.x2 - params.x1) / 2, params.y1,
                0,
                params.x1 + (params.x2 - params.x1) / 2, params.y1,
                math.max(params.x2 - params.x1, params.y2 - params.y1) / 2
            }
        elseif subparams.orientation == "bottom" then
            radial_params = {
                params.x1 + (params.x2 - params.x1) / 2, params.y2,
                0,
                params.x1 + (params.x2 - params.x1) / 2, params.y2,
                math.max(params.x2 - params.x1, params.y2 - params.y1) / 2
            }
        elseif subparams.orientation == "left" then
            radial_params = {
                params.x1, params.y1 + (params.y2 - params.y1) / 2,
                0,
                params.x1, params.y1 + (params.y2 - params.y1) / 2,
                math.max(params.x2 - params.x1, params.y2 - params.y1) / 2
            }
        elseif subparams.orientation == "right" then
            radial_params = {
                params.x2, params.y1 + (params.y2 - params.y1) / 2,
                0,
                params.x2, params.y1 + (params.y2 - params.y1) / 2,
                math.max(params.x2 - params.x1, params.y2 - params.y1) / 2
            }
        else
            radial_params = {
                params.x1 + (params.x2 - params.x1) / 2, params.y2 / 2,
                0,
                params.x1 + (params.x2 - params.x1) / 2, params.y2 / 2,
                math.max(params.x2 - params.x1, params.y2 - params.y1) / 2
            }
        end
        pattern = cairo_pattern_create_radial(table.unpack(radial_params))
    elseif subparams.subkind == "mesh" then
        pattern = cairo_pattern_create_mesh()
    elseif subparams.subkind == "linear" then
        -- vertical orientation for now
        -- we should really separate out the positioning info in later revisions
        lin_params = {}
        if subparams.orientation == "vertical" then
            line_params = {params.x1, params.y1, params.x1, params.y2}
        elseif subparams.orientation == "horizontal" then
            line_params = {params.x1, params.y1, params.x2, params.y1}
        elseif subparams.orientation == "lr" then
            line_params = {params.x1, params.y1, params.x2, params.y2}
        elseif subparams.orientation == "rl" then
            line_params = {params.x1, params.y2, params.x2, params.y1}
        else
            line_params = {params.x1, params.y1, params.x1, params.y2}
        end
        pattern = cairo_pattern_create_linear(table.unpack(line_params))
    else
        pattern = cairo_pattern_create_rgba(subparams.r, subparams.g, subparams.b, subparams.a)
    end

    if subparams.subkind == "linear" or subparams.subkind == "radial" then
        for index, entry in pairs(subparams.stops) do
            cairo_pattern_add_color_stop_rgba(pattern, entry.offset, entry.r, entry.g, entry.b, entry.a)
        end
    end
    return pattern
end

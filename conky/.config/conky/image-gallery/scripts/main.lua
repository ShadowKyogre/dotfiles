-- This is a lua script for use in Conky.
require 'cairo'
require 'cairo_imlib2_helper'
require 'math'
require 'os'

package.path = package.path .. ';' .. os.getenv('HOME') .. '/.config/conky/conky-utils/?.lua'
require 'graphics'

local pconfig = os.getenv('HOME') .. "/.config/conky/image-gallery/scripts/private.lua"
local ok, e = pcall(dofile, pconfig)
if not ok then
    imgs = {}
end
img_cache = {}

function stamp_image(cr, file, setup)
  cairo_save(cr)
  local cs = img_cache[file]
  local ref_count = cairo_surface_get_reference_count(cs)
  print(file .. " has " .. ref_count .. " references")
  if ref_count == 0 then
    img_cache[file] = surface_from_png(file, setup)
    cs = img_cache[file]
  end
  local x  = setup.x or 0
  local y  = setup.y or 0

  cairo_set_source_surface(cr, cs, x, y)
  cairo_paint_with_alpha(cr, 1)
  cairo_restore(cr)
end

function conky_startup()
    print("Initializing image cache...")
    for key, value in pairs(imgs) do
        print("Caching ".. value)
        img_cache[value] = surface_from_png(value, {smart=true, w=256, h=256})
    end
end

function conky_shutdown()
    print("Tearing down image cache...")
    for key, value in pairs(imgs) do
        cairo_surface_destroy(img_cache[value])
        img_cache[value] = nil
        print("Ripping up " .. value)
    end
    print("Done tearing up!")
end

function conky_main ()
    if conky_window == nil then
        return
    end
    local cs = cairo_xlib_surface_create (conky_window.display,
                                         conky_window.drawable,
                                         conky_window.visual,
                                         conky_window.width,
                                         conky_window.height)
    cr = cairo_create (cs)

    for key, value in pairs(imgs) do
        key_offset = 256 * (key - 1)
        -- print(key_offset, value)
        stamp_image(cr, value, {smart=true, w=256, h=256, x=key_offset, y=0})
    end

    cairo_destroy (cr)
    cairo_surface_destroy (cs)
    cr = nil
end

export PYTHONSTARTUP="${HOME}/.config/python/config.py"
export ZDOTDIR="${HOME}/.config/zsh"
export EDITOR=gvim
export PATH="${HOME}/bin:${HOME}/.pub-cache/bin/:/opt/flutter/bin/cache/dart-sdk/bin:${PATH}"
export VIEB_WINDOW_FRAME=1
[ -z "$SSH_AGENT_PID" ] && eval "$(ssh-agent -s)"
export SSH_ASKPASS=/usr/bin/ksshaskpass
